<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Penjualan</title>
</head>

<body>
    <p>
        <center><b>Data Laporan Penjualan</b></center>
    </p><br><br>

    <table border=1>
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-left">Nama Pembeli</th>
                <th class="text-left">Nama Kendaraan</th>
                <th class="text-left">Brand</th>
                <th class="text-left">Warna</th>
                <th class="text-left">Jenis</th>
                <th class="text-left">Tahun</th>
                <th class="text-left">Tipe Pembelian</th>
                <th class="text-left">Tanggal Penjualan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dt as $key => $value) { ?>
                <tr>
                    <td><?= $key+1 ?></td>
                    <td><?= $value->nama_pembeli ?></td>
                    <td><?= $value->nama ?></td>
                    <td><?= $value->brand ?></td>
                    <td><?= $value->warna ?></td>
                    <td><?= $value->jenis ?></td>
                    <td><?= $value->tahun ?></td>
                    <td><?= $value->tipe_pembelian ?></td>
                    <td><?= date_format(date_create($value->tanggal_penjualan), "d M Y") ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>