@extends("layout.header")
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Transaksi</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Menu Utama</a></div>
                <div class="breadcrumb-item">Data Transaksi Penjualan</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>List Transaksi Penjualan</h4>
                            <div class="card-header-form">
                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#mdlAdd">Tambah Transaksi</button>
                                <a target="__blank" href="/cetak_penjualan" class="btn btn-sm btn-success">Cetak Data Penjualan</a>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-left">Nama Pembeli</th>
                                        <th class="text-left">Nama Kendaraan</th>
                                        <th class="text-left">Brand</th>
                                        <th class="text-left">Warna</th>
                                        <th class="text-left">Jenis</th>
                                        <th class="text-left">Tahun</th>
                                        <th class="text-left">Tipe Pembelian</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    <?php if (!empty($dt_transaksi)) { ?>
                                        @foreach ($dt_transaksi as $k_transaksi => $v_transaksi)
                                        <tr>
                                            <td class="p-0 text-center">{{$k_transaksi+1}}</td>
                                            <td>{{$v_transaksi->nama_pembeli}}</td>
                                            <td>{{$v_transaksi->nama}}</td>
                                            <td>{{$v_transaksi->brand}}</td>
                                            <td>{{$v_transaksi->warna}}</td>
                                            <td>{{$v_transaksi->jenis}}</td>
                                            <td>{{$v_transaksi->tahun}}</td>
                                            <td class="text-uppercase">{{$v_transaksi->tipe_pembelian}}</td>
                                            <td class="text-center">
                                                <button class="btn btn-md btn-info" onclick="setUpdate('<?= base64_encode($v_transaksi->id) ?>')">Edit</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="9" class="text-secondary text-center">Data Kosong</td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal Add -->
    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="mdlAddLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-left: 5px solid #6777ef;">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlAddLabel">Input Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/save_transaksi" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Nama Pembeli</label>
                                    <input type="text" name="nama_pembeli" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Nomor Telepon/Hp</label>
                                    <input type="text" name="phone_pembeli" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" name="alamat_pembeli" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Tipe Pembelian</label>
                                    <select type="text" name="tipe_pembelian" class="form-control" required="">
                                        <option value="" selected>-- Pilih Jenis Pembelian --</option>
                                        <option value="cash">CASH</option>
                                        <option value="kredit">Kredit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Tanggal Transaksi</label>
                                    <input type="date" name="tanggal_penjualan" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Biaya Diterima</label>
                                    <input type="text" name="biaya" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Kendaraan</label>
                                    <select type="text" name="id_kendaraan" id="id_kendaraan_chs" class="form-control" onchange="setKendaraan('add')" required="">
                                        <option value="" selected>-- Pilih Kendaraan ---</option>
                                        <?php foreach ($data_kendaraan as $kdk => $valk) { ?>
                                            <option value="<?= $valk['id'] ?>"><?= $valk['nama'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsbrand d-none">
                                <div class="form-group">
                                    <label>Brand</label>
                                    <input type="text" name="brand" id="brand" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chswarna d-none">
                                <div class="form-group">
                                    <label>Warna</label>
                                    <input type="text" name="warna" id="warna" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsjenis d-none">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    <select type="text" name="jenis" id="jenis" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="MVP">MVP</option>
                                        <option value="SEDAN">SEDAN</option>
                                        <option value="SUV">SUV</option>
                                        <option value="HYBRID">HYBRID</option>
                                        <option value="SPORT">SPORT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chskondisi d-none">
                                <div class="form-group">
                                    <label>Kondisi</label>
                                    <select type="text" name="kondisi" id="kondisi" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Kondisi ---</option>
                                        <option value="po">Pre-Order</option>
                                        <option value="ready">Ready Stock</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chstransmisi d-none">
                                <div class="form-group">
                                    <label>Transmisi</label>
                                    <select type="text" name="transmisi" id="transmisi" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="Automatic">Automatic</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsbahanbakar d-none">
                                <div class="form-group">
                                    <label>Bahan Bakar</label>
                                    <select type="text" name="bahan_bakar" id="bahan_bakar" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Bahan Bakar ---</option>
                                        <option value="Bensin">Bensin</option>
                                        <option value="Solar">Solar</option>
                                        <option value="Diesel">Diesel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chstahun d-none">
                                <div class="form-group">
                                    <label>Tahun</label>
                                    <input type="year" name="tahun" id="tahun" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsharga d-none">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="text" name="harga" id="harga" class="form-control" required="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-block btn-secondary text-dark" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-md btn-block m-0 btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Edit -->
    <div class="modal fade" id="mdlUpdate" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="mdlUpdateLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-left: 5px solid #6777ef;">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlUpdateLabel">Update Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/update_transaksi" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Nama Pembeli</label>
                                    <input type="hidden" name="id_transaksi_upd" id="id_transaksi_upd" class="form-control" required="">
                                    <input type="text" name="upd_nama_pembeli" id="upd_nama_pembeli" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Nomor Telepon/Hp</label>
                                    <input type="text" name="upd_phone_pembeli" id="upd_phone_pembeli" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" name="upd_alamat_pembeli" id="upd_alamat_pembeli" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Tipe Pembelian</label>
                                    <select type="text" name="upd_tipe_pembelian" id="upd_tipe_pembelian" class="form-control" required="">
                                        <option value="" selected>-- Pilih Jenis Pembelian --</option>
                                        <option value="cash">CASH</option>
                                        <option value="kredit">Kredit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Tanggal Transaksi</label>
                                    <input type="date" name="upd_tanggal_penjualan" id="upd_tanggal_penjualan" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Biaya Diterima</label>
                                    <input type="text" name="upd_biaya" id="upd_biaya" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Kendaraan</label>
                                    <select type="text" name="upd_id_kendaraan" id="upd_id_kendaraan_chs" class="form-control" onchange="setKendaraan('update')" required="">
                                        <option value="" selected>-- Pilih Kendaraan ---</option>
                                        <?php foreach ($data_kendaraan as $kdk => $valk) { ?>
                                            <option value="<?= $valk['id'] ?>"><?= $valk['nama'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsbrand">
                                <div class="form-group">
                                    <label>Brand</label>
                                    <input type="text" name="upd_brand" id="upd_brand" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chswarna">
                                <div class="form-group">
                                    <label>Warna</label>
                                    <input type="text" name="upd_warna" id="upd_warna" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsjenis">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    <select type="text" name="upd_jenis" id="upd_jenis" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="MVP">MVP</option>
                                        <option value="SEDAN">SEDAN</option>
                                        <option value="SUV">SUV</option>
                                        <option value="HYBRID">HYBRID</option>
                                        <option value="SPORT">SPORT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chskondisi">
                                <div class="form-group">
                                    <label>Kondisi</label>
                                    <select type="text" name="upd_kondisi" id="upd_kondisi" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Kondisi ---</option>
                                        <option value="po">Pre-Order</option>
                                        <option value="ready">Ready Stock</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chstransmisi">
                                <div class="form-group">
                                    <label>Transmisi</label>
                                    <select type="text" name="upd_transmisi" id="upd_transmisi" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="Automatic">Automatic</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsbahanbakar">
                                <div class="form-group">
                                    <label>Bahan Bakar</label>
                                    <select type="text" name="upd_bahan_bakar" id="upd_bahan_bakar" class="form-control" required="" readonly>
                                        <option value="" selected>-- Pilih Bahan Bakar ---</option>
                                        <option value="Bensin">Bensin</option>
                                        <option value="Solar">Solar</option>
                                        <option value="Diesel">Diesel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chstahun">
                                <div class="form-group">
                                    <label>Tahun</label>
                                    <input type="year" name="upd_tahun" id="upd_tahun" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 chsharga">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="text" name="upd_harga" id="upd_harga" class="form-control" required="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-block btn-secondary text-dark" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-md btn-block m-0 btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Delete -->
    <div class="modal fade" id="mdlDelete" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="mdlDeleteLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-left: 5px solid #6777ef;">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlDeleteLabel">Delete Kendaraan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/delete_kendaraan" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <input type="text" name="id_kendaraan_del" id="id_kendaraan_del" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-block btn-secondary text-dark" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-md btn-block m-0 btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function setUpdate(id) {
            $("#mdlUpdate").modal("show");
            $.ajax({
                url: '/get_transaksi',
                method: 'GET',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    
                    $("#id_transaksi_upd").val(data.id);
                    $("#upd_nama_pembeli").val(data.nama_pembeli);
                    $("#upd_phone_pembeli").val(data.phone_pembeli);
                    $("#upd_alamat_pembeli").val(data.alamat_pembeli);
                    $("#upd_tipe_pembelian").val(data.tipe_pembelian);
                    $("#upd_tanggal_penjualan").val("2022-05-20");
                    $("#upd_biaya").val(data.biaya);
                    getUpdateKendaraan(data.id_kendaraan)
                }
            });
        }

        function getUpdateKendaraan(id_kendaraan) {
            $.ajax({
                url: '/get_kendaraan',
                method: 'GET',
                data: {
                    id: btoa(id_kendaraan)
                },
                dataType: 'json',
                success: function(data) {
                    
                    $("#upd_id_kendaraan_chs").val(data.id);
                    $("#upd_brand").val(data.brand);
                    $("#upd_warna").val(data.warna);
                    $("#upd_jenis").val(data.jenis);
                    $("#upd_kondisi").val(data.kondisi == "Ready Stock" ? "ready" : "po");
                    $("#upd_transmisi").val(data.transmisi);
                    $("#upd_bahan_bakar").val(data.bahan_bakar);
                    $("#upd_tahun").val(data.tahun);
                    $("#upd_harga").val(data.harga);
                }
            });
        }

        function setKendaraan(type) {
            $.ajax({
                url: '/get_kendaraan',
                method: 'GET',
                data: {
                    id: type == 'add' ? btoa($("#id_kendaraan_chs").val()) : btoa($("#upd_id_kendaraan_chs").val())
                },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    $("#id_kendaraan").val(data.id);
                    $(".chsbrand").removeClass("d-none");
                    $(".chswarna").removeClass("d-none");
                    $(".chsjenis").removeClass("d-none");
                    $(".chskondisi").removeClass("d-none");
                    $(".chstransmisi").removeClass("d-none");
                    $(".chsbahanbakar").removeClass("d-none");
                    $(".chstahun").removeClass("d-none");
                    $(".chsharga").removeClass("d-none");
                    
                    $("#brand").val(data.brand);
                    $("#warna").val(data.warna);
                    $("#jenis").val(data.jenis);
                    $("#kondisi").val(data.kondisi == "Ready Stock" ? "ready" : "po");
                    $("#transmisi").val(data.transmisi);
                    $("#bahan_bakar").val(data.bahan_bakar);
                    $("#tahun").val(data.tahun);
                    $("#harga").val(data.harga);
                }
            });
        }
    </script>
</div>
@endsection