@extends("layout.header")
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Kendaraan</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Menu Utama</a></div>
                <div class="breadcrumb-item">Data Kendaraan</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4></h4>
                            <div class="card-header-form">
                                <button class="btn btn-sm btn-primary" data-toggle="modal"data-target="#mdlAdd">Tambah Data</button>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-left">Nama</th>
                                        <th class="text-left">Brand</th>
                                        <th class="text-left">Warna</th>
                                        <th class="text-left">Jenis</th>
                                        <th class="text-left">Kondisi</th>
                                        <th class="text-left">Transmisi</th>
                                        <th class="text-left">Tahun</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    <?php if (!empty($dt_kendaraan)) { ?>
                                        @foreach ($dt_kendaraan as $k_kendaraan => $v_kendaraan)
                                        <tr>
                                            <td class="p-0 text-center">{{$k_kendaraan+1}}</td>
                                            <td>{{$v_kendaraan['nama']}}</td>
                                            <td>{{$v_kendaraan['brand']}}</td>
                                            <td>{{$v_kendaraan['warna']}}</td>
                                            <td>{{$v_kendaraan['jenis']}}</td>
                                            <td>{{$v_kendaraan['kondisi']}}</td>
                                            <td>{{$v_kendaraan['transmisi']}}</td>
                                            <td>{{$v_kendaraan['tahun']}}</td>
                                            <td class="text-center">
                                                <button class="btn btn-sm btn-info" onclick="setUpdate('<?= base64_encode($v_kendaraan['id']) ?>')">Edit</button>
                                                <button class="btn btn-sm btn-danger" onclick="setDelete('<?= base64_encode($v_kendaraan['id']) ?>')">Delete</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="9" class="text-secondary text-center">Data Kosong</td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal Add -->
    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="mdlAddLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-left: 5px solid #6777ef;">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlAddLabel">Input Data Kendaraan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/save_kendaraan" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Nama Kendaraan</label>
                                    <input type="text" name="nama_kendaraan" id="nama_kendaraan" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Brand</label>
                                    <input type="text" name="brand" id="brand" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Warna</label>
                                    <input type="text" name="warna" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    <select type="text" name="jenis" class="form-control" required="">
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="MVP">MVP</option>
                                        <option value="SEDAN">SEDAN</option>
                                        <option value="SUV">SUV</option>
                                        <option value="HYBRID">HYBRID</option>
                                        <option value="SPORT">SPORT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Kondisi</label>
                                    <select type="text" name="kondisi" class="form-control" required="">
                                        <option value="" selected>-- Pilih Kondisi ---</option>
                                        <option value="po">Pre-Order</option>
                                        <option value="ready">Ready Stock</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Transmisi</label>
                                    <select type="text" name="transmisi" class="form-control" required="">
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="Automatic">Automatic</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Bahan Bakar</label>
                                    <select type="text" name="bahan_bakar" class="form-control" required="">
                                        <option value="" selected>-- Pilih Bahan Bakar ---</option>
                                        <option value="Bensin">Bensin</option>
                                        <option value="Solar">Solar</option>
                                        <option value="Diesel">Diesel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Tahun</label>
                                    <input type="year" name="tahun" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="text" name="harga" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-block btn-secondary text-dark" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-md btn-block m-0 btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Edit -->
    <div class="modal fade" id="mdlUpdate" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="mdlUpdateLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-left: 5px solid #6777ef;">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlUpdateLabel">Edit Kendaraan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/update_kendaraan" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Nama Kendaraan</label>
                                    <input type="hidden" name="id_kendaraan" id="id_kendaraan" class="form-control" required="">
                                    <input type="text" name="upd_nama_kendaraan" id="upd_namker" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Brand</label>
                                    <input type="text" name="upd_brand" id="upd_brand" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Warna</label>
                                    <input type="text" name="upd_warna" id="upd_warna" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    <select type="text" name="upd_jenis" id="upd_jenis" class="form-control" required="">
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="MVP">MVP</option>
                                        <option value="SEDAN">SEDAN</option>
                                        <option value="SUV">SUV</option>
                                        <option value="HYBRID">HYBRID</option>
                                        <option value="SPORT">SPORT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Kondisi</label>
                                    <select type="text" name="upd_kondisi" id="upd_kondisi" class="form-control" required="">
                                        <option value="" selected>-- Pilih Kondisi ---</option>
                                        <option value="po">Pre-Order</option>
                                        <option value="ready">Ready Stock</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Transmisi</label>
                                    <select type="text" name="upd_transmisi" id="upd_transmisi" class="form-control" required="">
                                        <option value="" selected>-- Pilih Jenis ---</option>
                                        <option value="Automatic">Automatic</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Bahan Bakar</label>
                                    <select type="text" name="upd_bahan_bakar" id="upd_bahan_bakar" class="form-control" required="">
                                        <option value="" selected>-- Pilih Bahan Bakar ---</option>
                                        <option value="Bensin">Bensin</option>
                                        <option value="Solar">Solar</option>
                                        <option value="Diesel">Diesel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Tahun</label>
                                    <input type="year" name="upd_tahun" id="upd_tahun" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="text" name="upd_harga" id="upd_harga" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-block btn-secondary text-dark" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-md btn-block m-0 btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Delete -->
    <div class="modal fade" id="mdlDelete" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="mdlDeleteLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="border-left: 5px solid #d63031;">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlDeleteLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/delete_kendaraan" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h6 class="text-center">Apakah anda yakin ingin menghapus data ini ?</h6>
                                <div class="form-group">
                                    <input type="hidden" name="id_kendaraan_del" id="id_kendaraan_del" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-md btn-block btn-secondary text-dark" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-md btn-block m-0 btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function setUpdate(id) {
            $("#mdlUpdate").modal("show");
            $.ajax({
                url: '/get_kendaraan',
                method: 'GET',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    $("#id_kendaraan").val(data.id);
                    $("#upd_namker").val(data.nama);
                    $("#upd_brand").val(data.brand);
                    $("#upd_warna").val(data.warna);
                    $("#upd_jenis").val(data.jenis);
                    $("#upd_kondisi").val(data.kondisi == "Ready Stock" ? "ready" : "po");
                    $("#upd_transmisi").val(data.transmisi);
                    $("#upd_bahan_bakar").val(data.bahan_bakar);
                    $("#upd_tahun").val(data.tahun);
                    $("#upd_harga").val(data.harga);
                }
            });
        }

        function setDelete(id) {
            $("#mdlDelete").modal("show");
            $.ajax({
                url: '/get_kendaraan',
                method: 'GET',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    $("#id_kendaraan_del").val(data.id);
                }
            });
        }
    </script>
</div>
@endsection