<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\Kendaraan;
use Illuminate\Support\Facades\DB;

use Barryvdh\DomPDF\Facade as PDF;

class TransaksiController extends Controller
{
    public function index()
    {
        $data_kendaraan = Kendaraan::get()->toArray();
        $dt_transaksi = DB::table("transaksis")
                            ->select('transaksis.*', 'kendaraans.nama', 'kendaraans.brand', 'kendaraans.warna', 'kendaraans.jenis', 'kendaraans.tahun')
                            ->join("kendaraans", "kendaraans.id", "=", "transaksis.id_kendaraan")->get()->toArray();
        
        foreach ($dt_transaksi as $key => $value) {
            $dt_transaksi[$key]->tanggal_penjualan = date_format(date_create($value->tanggal_penjualan), "Y-m-d");
        }
        

        return view('transaksi.index', compact("dt_transaksi","data_kendaraan"));
    }

    public function save_transaksi(Request $request)
    {
        $transaksi = new Transaksi();

        $transaksi->id_kendaraan = $request->id_kendaraan;
        $transaksi->nama_pembeli = $request->nama_pembeli;
        $transaksi->phone_pembeli = $request->phone_pembeli;
        $transaksi->alamat_pembeli = $request->alamat_pembeli;
        $transaksi->tipe_pembelian = $request->tipe_pembelian;
        $transaksi->tanggal_penjualan = $request->tanggal_penjualan;
        $transaksi->biaya = $request->biaya;
        $transaksi->created_by = auth()->user()->name;
        $transaksi->created_at = date("Y-m-d H:i:s");

        $transaksi->save();

        return redirect('/transaksi')->with(['success' => 'transaksi Berhasil Disimpan']);
    }

    public function get_transaksi(Request $request)
    {
        $dt_transaksi = Transaksi::find(base64_decode($request->id));
        
        
        return $dt_transaksi;
    }

    public function update_transaksi(Request $request)
    {
        $transaksi = Transaksi::find($request->id_transaksi_upd);
       
        $transaksi->id_kendaraan = $request->upd_id_kendaraan;
        $transaksi->nama_pembeli = $request->upd_nama_pembeli;
        $transaksi->phone_pembeli = $request->upd_phone_pembeli;
        $transaksi->alamat_pembeli = $request->upd_alamat_pembeli;
        $transaksi->tipe_pembelian = $request->upd_tipe_pembelian;
        $transaksi->tanggal_penjualan = $request->upd_tanggal_penjualan;
        $transaksi->biaya = $request->upd_biaya;
        $transaksi->updated_by = auth()->user()->name;
        $transaksi->updated_at = date("Y-m-d H:i:s");

        $transaksi->save();

        return redirect('/transaksi')->with(['success' => 'transaksi Berhasil Disimpan']);
    }

    public function delete_transaksi(Request $request)
    {
        $transaksi = Transaksi::find($request->id_transaksi_del);

        $transaksi->delete();

        return redirect('/dashboard')->with(['success' => 'Data transaksi berhasil dihapus']);
    }

    public function cetak_penjualan(Request $request)
    {
        $dt = DB::table("transaksis")
                    ->select('transaksis.*', 'kendaraans.nama', 'kendaraans.brand', 'kendaraans.warna', 'kendaraans.jenis', 'kendaraans.tahun')
                    ->join("kendaraans", "kendaraans.id", "=", "transaksis.id_kendaraan")->get()->toArray();
                    
        $dompdf=app('dompdf.wrapper');
        $dompdf->getDomPDF()->set_option("enable_php", true);
        $dompdf->loadView('transaksi.laporan',compact('dt'));
        return $dompdf->stream('Laporan Penjualan.pdf',array("Attachment"=>false));
    }
}
