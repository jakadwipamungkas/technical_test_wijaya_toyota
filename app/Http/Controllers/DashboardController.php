<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kendaraan;

class DashboardController extends Controller
{
    public function index()
    {
        $dt_kendaraan = Kendaraan::get()->toArray();

        return view('dashboard.index', compact("dt_kendaraan"));
    }

    public function save_kendaraan(Request $request)
    {
        // print_r("<pre>");
        // print_r($request->all());
        // exit();
        $kendaraan = new Kendaraan();

        $kendaraan->nama        = $request->nama_kendaraan;
        $kendaraan->brand       = $request->brand;
        $kendaraan->warna       = $request->warna;
        $kendaraan->jenis       = $request->jenis;
        $kendaraan->tahun       = $request->tahun;
        $kendaraan->bahan_bakar = $request->bahan_bakar;
        $kendaraan->harga       = $request->harga;
        $kendaraan->kondisi     = $request->kondisi == "ready" ? "Ready Stock" : "Pre-Order";
        $kendaraan->transmisi   = $request->transmisi;
        $kendaraan->created_by  = auth()->user()->name;
        $kendaraan->created_at  = date("Y-m-d H:i:s");

        $kendaraan->save();

        return redirect('/dashboard')->with(['success' => 'Kendaraan Berhasil Disimpan']);
    }

    public function get_kendaraan(Request $request)
    {

        $dt_Kendaraan = Kendaraan::find(base64_decode($request->id));
        
        return $dt_Kendaraan;
    }

    public function update_kendaraan(Request $request)
    {
        $kendaraan = Kendaraan::find($request->id_kendaraan);
        $kendaraan->nama        = $request->upd_nama_kendaraan;
        $kendaraan->brand       = $request->upd_brand;
        $kendaraan->warna       = $request->upd_warna;
        $kendaraan->jenis       = $request->upd_jenis;
        $kendaraan->tahun       = $request->upd_tahun;
        $kendaraan->bahan_bakar = $request->upd_bahan_bakar;
        $kendaraan->harga       = $request->upd_harga;
        $kendaraan->kondisi     = $request->upd_kondisi == "ready" ? "Ready Stock" : "Pre-Order";
        $kendaraan->transmisi   = $request->upd_transmisi;
        $kendaraan->updated_by  = auth()->user()->name;
        $kendaraan->updated_at  = date("Y-m-d H:i:s");

        $kendaraan->save();

        return redirect('/dashboard')->with(['success' => 'Kendaraan Berhasil Diupdate']);
    }

    public function delete_kendaraan(Request $request)
    {
        $kendaraan = Kendaraan::find($request->id_kendaraan_del);

        $kendaraan->delete();

        return redirect('/dashboard')->with(['success' => 'Data Kendaraan berhasil dihapus']);
    }
}
