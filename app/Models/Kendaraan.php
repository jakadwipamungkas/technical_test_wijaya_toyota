<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama', 'brand', 'warna', 'kondisi', 'jenis', 'transmisi', 'tahun', 'harga', 'bahan_bakar',
    ];
}
