<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware("auth");
Route::post('/save_kendaraan', [DashboardController::class, 'save_kendaraan']);
Route::post('/update_kendaraan', [DashboardController::class, 'update_kendaraan']);
Route::post('/delete_kendaraan', [DashboardController::class, 'delete_kendaraan']);
Route::get('/get_kendaraan', [DashboardController::class, 'get_kendaraan']);


Route::get('/transaksi', [TransaksiController::class, 'index'])->middleware("auth");
Route::post('/save_transaksi', [TransaksiController::class, 'save_transaksi']);
Route::post('/update_transaksi', [TransaksiController::class, 'update_transaksi']);
Route::get('/get_transaksi', [TransaksiController::class, 'get_transaksi']);
Route::get('/cetak_penjualan', [TransaksiController::class, 'cetak_penjualan']);